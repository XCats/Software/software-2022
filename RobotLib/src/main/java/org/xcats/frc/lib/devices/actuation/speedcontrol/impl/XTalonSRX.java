package org.xcats.frc.lib.devices.actuation.speedcontrol.impl;

import org.xcats.frc.lib.logging.XLogString;

import edu.wpi.first.wpilibj.motorcontrol.Talon;

@Deprecated
public class XTalonSRX extends XGenericSpeedController<Talon> {

	private final int mPwmChannel;

	public XTalonSRX(String name, int pwmChannel) {
		super(name);
		this.mPwmChannel = pwmChannel;
	}

	@Override
	public void initialize() {
		super.mMotor = new Talon(this.mPwmChannel);
	}

	@Override
	public XLogString getLogString() {
		return null;
	}
}
