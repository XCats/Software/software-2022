package org.xcats.frc.lib.control;

/**
 * Interface for any controller with a setpoint
 */
public interface XController {
	public void alterSetPoint(double newSetPoint);

	public double getSetPoint();
}
