package org.xcats.frc.lib.control.impl;

import org.xcats.frc.lib.control.XBaseController;
import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedController;
import org.xcats.frc.lib.devices.sensors.XSensor;

public class XPController extends XBaseController {

	public XPController(XSpeedController motor, XSensor<Double> encoder, double coefficient) {
		super(motor, encoder, new XAlgorithm() {
			@Override
			public void exec() {
				double newSpeed = this.calcP(coefficient, encoder.getCurrentValue());
				motor.alterSetPoint(newSpeed);
			}

			private double calcP(double coefficient, double value) {
				return coefficient * (this.mSetPoint - value);
			}
		});
	}
}
