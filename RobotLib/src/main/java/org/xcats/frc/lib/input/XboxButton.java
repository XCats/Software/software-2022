package org.xcats.frc.lib.input;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.button.Button;

public class XboxButton extends Button {
    private final XboxController mOperator;
    private final XboxController.Button mButton;


    public XboxButton(XboxController operator, XboxController.Button button) {
        this.mOperator = operator;
        this.mButton = button;
    }

	@Override
    public boolean get() {
        return mOperator.getRawButton(mButton.value);

    }
}