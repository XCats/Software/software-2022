package org.xcats.frc.lib.devices.sensors.impl;

import java.util.function.DoubleSupplier;

import org.xcats.frc.lib.devices.sensors.XInu;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;

public abstract class XBaseInu implements XInu {

	private final DoubleSupplier mActualYawSupplier;
	private final NetworkTable mNetworkTable;
	private double mStartingAngle;

	public XBaseInu(String name, GyroDirection gyroDirection) {
		mActualYawSupplier = getSupplier(gyroDirection);
		mStartingAngle = 0;
		mNetworkTable = NetworkTableInstance.getDefault().getTable(name);
	}

	@Override
	public double getYaw() {
		return mActualYawSupplier.getAsDouble() - mStartingAngle;
	}

	@Override
	public void setYaw(double angle) {
		mStartingAngle = mActualYawSupplier.getAsDouble() - angle;
	}

	@Override
	public void pushNetworkTables() {
		mNetworkTable.getEntry("Raw Yaw").setDouble(getRawYaw());
		mNetworkTable.getEntry("Raw Pitch").setDouble(getRawPitch());
		mNetworkTable.getEntry("Raw Roll").setDouble(getRawRoll());
		mNetworkTable.getEntry("Yaw").setDouble(getYaw());
	}

	protected final DoubleSupplier getSupplier(GyroDirection gyroDirection) {

		switch (gyroDirection) {
			case Yaw:
				return this::getRawYaw;
			case Pitch:
				return this::getRawPitch;
			case Roll:
				return this::getRawRoll;
			default:
				throw new IllegalArgumentException();
		}
	}
}
