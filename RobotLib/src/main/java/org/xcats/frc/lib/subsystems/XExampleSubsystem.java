package org.xcats.frc.lib.subsystems;

import org.xcats.frc.lib.XSubsystem;
import org.xcats.frc.lib.devices.actuation.XActuator;
import org.xcats.frc.lib.devices.sensors.XSensor;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;

/**
 * This class provides an example implementation of the XSubsystem interface. It
 * implements a simple claw with acquisition wheels. The claw is either open or
 * closed, and the acquisition wheels can be set to run at a given RPM. There is
 * also a sensor for if a ball is in the acquisition. It uses a controller /
 * state design pattern as well as a singleton design pattern.
 */
@SuppressWarnings("PMD")
public class XExampleSubsystem extends XSubsystem {
	/**
	 * A controller class that contains all variables that are modified to control
	 * the subsystem
	 */
	public class Controller {
		private boolean mClawOpen;
		private double mAcquisitionRpm;
		private boolean mBallHeld;

		/**
		 * Changes the claw state (from open to closed or vice-versa)
		 */
		public void changeClawState() {
			this.mClawOpen = !mClawOpen;
		}

		/**
		 * Sets the RPM for the acquisition wheels on the claw
		 * 
		 * @param newRpm the new RPM
		 */
		public void setAcquisitionRpm(double newRpm) {
			this.mAcquisitionRpm = newRpm;
		}

		public void eject() {
			this.mBallHeld = false;
		}
	}

	/**
	 * A state class is built that will represent the state of the subsystem
	 */
	public class State {
		private boolean mClawOpen;
		private double mAcquisitionRpm;
		private boolean mBallHeld;
	}

	// Holds the desired state of the subsystem
	private State mDesiredState;

	// Holds the actual state of the subsystem
	private State mCurrentState;

	private Controller mController;

	// Injected Dependencies
	private MotorController mAcquisitionSpeedController;
	private XSensor<Boolean> mBallSensor;
	private XActuator mActuator;

	public XExampleSubsystem(MotorController acquisitionSpeedController, XSensor<Boolean> ballSensor, XActuator actuator) {
		this.mAcquisitionSpeedController = acquisitionSpeedController;
		this.mBallSensor = ballSensor;
		this.mActuator = actuator;
	}

	public State getDesiredState() {
		return this.mDesiredState;
	}

	public State getCurrentState() {
		return this.mCurrentState;
	}

	public Controller getController() {
		return this.mController;
	}

	@Override
	public void stop() {
		// No motion state here
	}

	protected void putNetworkTableInfo() {
		
	}


	// @Override
	// public XLogString getLogString() {
	// 	return null;
	// }

	public void periodic() {
		super.periodic();

		// Read values from controller into the desired state
		this.mDesiredState.mClawOpen = this.getController().mClawOpen;
		this.mDesiredState.mAcquisitionRpm = this.getController().mAcquisitionRpm;
		this.mDesiredState.mBallHeld = this.getController().mBallHeld;

		// Read value from ball sensor into current state (because this is current)
		this.mCurrentState.mBallHeld = this.mBallSensor.getCurrentValue();
	}


}
