package org.xcats.frc.lib.subsystems.impl;

import org.xcats.frc.lib.XSubsystem;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;

/**
 * This class provides a basic implementation of a tank drive. It uses two speed controllers, one for wheels on the
 * left side of a drive base, and one for wheels on the right side. These speed controllers can be master speed controllers,
 * utilizing functionality that already exists in classes such as {@link com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX},
 * or they could be {@link edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup}s.
 */
public abstract class DriveBase extends XSubsystem {

	/**
	 * Initialize a new drive base with left and right motors as well as encoders
	 * 
	 * @param leftMotor  The left motor(s). Can be an individual speedcontroller or
	 *                   a {@link edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup}
	 * @param rightMotor The right motor(s). Can be an individual speedcontroller or
	 *                   a {@link edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup}
	 */
	public DriveBase(MotorController leftMotor, MotorController rightMotor) {
		this.mLeftMotor = leftMotor;
		this.mRightMotor = rightMotor;
	}

	/**
	 * A controller class that contains all variables that are modified to control
	 * the subsystem
	 */
	public static class Controller {
		private double mRightSpeed;
		private double mLeftSpeed;

		/**
		 * Set the speed of the motors on the right side of the drive base
		 * @param speed the new motor speed (-1 to 1)
		 */
		public void setRightSpeed(double speed) {
			this.mRightSpeed = speed;
		}

		/**
		 * Set the speed of the motors on the left side of the drive base
		 * @param speed the new motor speed (-1 to 1)
		 */
		public void setLeftSpeed(double speed) {
			this.mLeftSpeed = speed;
		}
	}

	/**
	 * A state class is built that will represent the state of the subsystem
	 */
	public class State {
		private double mRightSpeed;
		private double mLeftSpeed;

		/**
		 * Get the speed of the motors on the right side of the drive base
		 * @return the speed of the motors
		 */
		public double getRightSpeed() {
			return mRightSpeed;
		}

		/**
		 * Get the speed of the motors on the left side of the drive base
		 * @return the speed of the motors
		 */
		public double getLeftSpeed() {
			return mLeftSpeed;
		}
	}

	// Used to control the subsystem
	private final Controller mController = new Controller();

	// Member variables
	protected final MotorController mLeftMotor;
	protected final MotorController mRightMotor;

	/**
	 * Gets the desired state of the drive base, as determined from the controller
	 * 
	 * @return a {@link State} object holding the desired state of the drive base
	 */
	public abstract State getDesiredState();

	/**
	 * Gets the current state of the drive base, as determined by what the motors are currently set to
	 * 
	 * @return a {@link State} object holding the current state of the drive base
	 */
	public abstract State getCurrentState();

	/**
	 * Gets the controller for the drive base, which can then be used for all control of the Drive base
	 * 
	 * @return the controller object for the subsystem
	 */
	public Controller getController() {
		return this.mController;
	}

	@Override
	public void stop() {
		this.mLeftMotor.stopMotor();
		this.mRightMotor.stopMotor();
	}

	/**
	 * Read left and right speed values from the controller into the desired state.
	 */
	public void getPeriodicInput() {
		// Read values from the controller into the desired state
		this.getDesiredState().mLeftSpeed = this.mController.mLeftSpeed;
		this.getDesiredState().mRightSpeed = this.mController.mRightSpeed;
	}

	/**
	 * Writes the new speeds to the current state, and sets the left and right motor speeds according to the desired
	 * state.
	 */
	public void writePeriodicOutput() {
		// Normally would check if the states are different, but here we need to always
		// send a setpoint anyways


		this.mRightMotor.set(this.getDesiredState().mRightSpeed);
		this.mLeftMotor.set(this.getDesiredState().mLeftSpeed);

		this.getCurrentState().mRightSpeed = this.mRightMotor.get();
		this.getCurrentState().mLeftSpeed = this.mLeftMotor.get();
	}
}
