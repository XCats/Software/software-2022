package org.xcats.frc.lib.devices.actuation.speedcontrol.impl;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedController;
import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedControllerStatus;

import edu.wpi.first.wpilibj.SpeedController;

/**
 * Defines an implementation of {@link XSpeedController} for use with any mMotor
 * that implements the {@link SpeedController} interface defined in the WPILib
 */
@Deprecated
public abstract class XGenericSpeedController<S extends MotorController> implements XSpeedController {
	protected S mMotor;
	private final String mName;
	private boolean mInverted;
	private double mSetPoint;

	private int mInvertCoefficient = 1;

	protected XGenericSpeedController(String name) {
		this.mName = name;
	}

	@Override
	public String getName() {
		return this.mName;
	}

	@Override
	public void setInverted() {
		this.mInverted = !this.mInverted;
		this.mInvertCoefficient = -1 * this.mInvertCoefficient;
	}

	public S getMotor() {
		return this.mMotor;
	}

	@Override
	public void stop() {
		this.alterSetPoint(0);
	}

	@Override
	public boolean isInverted() {
		return this.mInverted;
	}

	@Override
	public void alterSetPoint(double newSetPoint) {
		this.mSetPoint = newSetPoint;
		mMotor.set(mSetPoint * mInvertCoefficient);
	}

	@Override
	public XSpeedControllerStatus getStatus() {
		return null;
	}

	@Override
	public double getSetPoint() {
		return this.mSetPoint;
	}
}
