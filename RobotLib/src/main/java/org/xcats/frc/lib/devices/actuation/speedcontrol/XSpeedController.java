package org.xcats.frc.lib.devices.actuation.speedcontrol;

import org.xcats.frc.lib.devices.XControlDevice;

/**
 * This interface defines a set of methods, most of which were extracted from
 * {@link org.xcats.deprecated.XCatsSpeedController}, that define
 * what all speed controllers should do. This interface is hardware agnostic,
 * meaning that for any implementation of a protocol such as CAN or PWM a new
 * interface should be constructed for any hardware-specific features.
 */
@Deprecated
public interface XSpeedController extends XControlDevice {
	/**
	 * Set whether or not the direction the speed controller drives the mMotor is
	 * inverted.
	 */
	public void setInverted();

	/**
	 *
	 * @return whether or not the speed controller's direction is inverted
	 */
	public boolean isInverted();

	public XSpeedControllerStatus getStatus();

}
