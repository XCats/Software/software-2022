package org.xcats.frc.lib.processors;

import org.xcats.frc.lib.XLoggable;
import org.xcats.frc.lib.XLoopable;

public interface XProcessor extends XLoggable, XLoopable {
}
