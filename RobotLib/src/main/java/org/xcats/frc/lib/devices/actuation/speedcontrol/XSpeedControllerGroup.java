package org.xcats.frc.lib.devices.actuation.speedcontrol;

import java.util.List;

@Deprecated
public interface XSpeedControllerGroup<S extends XSpeedController> extends XSpeedController {
	public List<S> getFollowers();

	public S getMaster();
}
