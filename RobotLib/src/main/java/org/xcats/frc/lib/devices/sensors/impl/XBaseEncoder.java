package org.xcats.frc.lib.devices.sensors.impl;

import org.xcats.frc.lib.devices.sensors.XEncoder;

import edu.wpi.first.util.sendable.Sendable;
import edu.wpi.first.util.sendable.SendableBuilder;

public abstract class XBaseEncoder implements XEncoder {

	private final String mEncoderType;
	private final Units mUnitsText;
	private final String mName;
	private final double mDistanceTicksPerRotationUnit;
	private final double mVelocityTicksPerVelocityUnit;
	private final double mDistancePerRotation;
	private XEncoderSendable mSendable;

	public XBaseEncoder(String name, String encoderType, double encoderTicksPerRevolution, double distancePerRotation,
			Units unitsText) {
		this(name, encoderType, encoderTicksPerRevolution, encoderTicksPerRevolution, distancePerRotation, unitsText);
	}

	public XBaseEncoder(String name, String encoderType, double distanceTicksPerRotation,
			double distanceTicksPerVelocity,
			double distancePerRotation,
			Units unitsText) {
		mName = name;
		mDistanceTicksPerRotationUnit = distanceTicksPerRotation;
		mVelocityTicksPerVelocityUnit = distanceTicksPerVelocity;
		mDistancePerRotation = distancePerRotation;
		mUnitsText = unitsText;
		mEncoderType = encoderType;
	}

	@Override
	public double getRotationalDistance() {
		return getRawDistanceTicks() / mDistanceTicksPerRotationUnit;
	}

	@Override
	public double getRotationalVelocity() {
		return getRawVelocityTicks() / mVelocityTicksPerVelocityUnit;
	}

	@Override
	public double getRPM() {
		return getRotationalVelocity() * 60;
	}

	@Override
	public double getLinearDistance() {
		return getRotationalDistance() * mDistancePerRotation;
	}

	@Override
	public double getLinearVelocity() {
		return getRotationalVelocity() * mDistancePerRotation;
	}

	@Override
	public Units getDistanceUnit() {
		return mUnitsText;
	}

	@Override
	public double convertToTicksFromDistance(double distance) {
		return (mDistanceTicksPerRotationUnit * distance) / mDistancePerRotation;
	}

	@Override
	public Sendable getSendable() {
		if (mSendable == null) {
			mSendable = new XEncoderSendable();
		}

		return mSendable;
	}

	private class XEncoderSendable implements Sendable {

		@Override
		public void initSendable(SendableBuilder builder) {
			builder.addStringProperty("Name", () -> mName, null);
			builder.addStringProperty("Encoder Type", () -> mEncoderType, null);
			builder.addDoubleProperty("Raw Distance", XBaseEncoder.this::getRawDistanceTicks, null);
			builder.addDoubleProperty("Raw Velocity", XBaseEncoder.this::getRawVelocityTicks, null);
			builder.addDoubleProperty("Rotations", XBaseEncoder.this::getRotationalDistance, null);
			builder.addDoubleProperty("Rotations per second", XBaseEncoder.this::getRotationalVelocity, null);
			builder.addDoubleProperty("Distance (" + mUnitsText + ")", XBaseEncoder.this::getLinearDistance, null);
			builder.addDoubleProperty("Velocity (" + mUnitsText + " per second)", XBaseEncoder.this::getLinearVelocity, null);
			builder.addDoubleProperty("Velocity (RPM)", XBaseEncoder.this::getRPM, null);
		}
	}

}

