package org.xcats.frc.lib.devices.actuation.speedcontrol.impl;

import org.xcats.frc.lib.devices.actuation.speedcontrol.XBrakeMode;
import org.xcats.frc.lib.devices.actuation.speedcontrol.XCANSpeedController;
import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedControllerStatus;
import org.xcats.frc.lib.logging.XLogString;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

@Deprecated
public class XCANTalonSRX extends XGenericSpeedController<WPI_TalonSRX> implements XCANSpeedController {
	private final int mChannel;

	private XBrakeMode mBrakeMode;

	public XCANTalonSRX(String name, int channel) {
		super(name);
		this.mChannel = channel;
	}

	@Override
	public XSpeedControllerStatus getStatus() {
		return null;
	}

	@Override
	public int getChannel() {
		return this.mChannel;
	}

	@Override
	public void setBrakeMode(XBrakeMode brakeMode) {
		this.mBrakeMode = brakeMode;
		super.mMotor.setNeutralMode(brakeMode.toNeutralMode());
	}

	@Override
	public XBrakeMode getBrakeMode() {
		return this.mBrakeMode;
	}

	@Override
	public void setRampingRate(double timeToFullSpeed) {
		super.mMotor.configOpenloopRamp(timeToFullSpeed, 0);
	}

	@Override
	public WPI_TalonSRX getMotor() {
		return this.mMotor;
	}

	@Override
	public void initialize() {
		super.mMotor = new WPI_TalonSRX(mChannel);
	}

	@Override
	public XLogString getLogString() {
		return null;
	}
}
