package org.xcats.frc.lib.devices.actuation.speedcontrol;

import com.ctre.phoenix.motorcontrol.NeutralMode;

@Deprecated
public enum XBrakeMode {
	BRAKE_MODE, COAST_MODE;

	public NeutralMode toNeutralMode() {
		switch (this) {
			case BRAKE_MODE:
				return NeutralMode.Brake;
			case COAST_MODE:
				return NeutralMode.Coast;
			default:
				return null;
		}

	}
}
