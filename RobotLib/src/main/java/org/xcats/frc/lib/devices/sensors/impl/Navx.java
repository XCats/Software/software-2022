package org.xcats.frc.lib.devices.sensors.impl;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.SPI;

public class Navx extends XBaseInu {
	private final AHRS mAhrs;

	public Navx() {
		this(GyroDirection.Yaw);
	}

	public Navx(GyroDirection gyroDirection) {
		super("Navx", gyroDirection);
		mAhrs = new AHRS(SPI.Port.kMXP);
	}


	@Override
	public double getRawPitch() {
		return mAhrs.getPitch();
	}

	@Override
	public double getRawRoll() {
		return mAhrs.getRoll();
	}

	@Override
	public double getRawYaw() {
		return mAhrs.getYaw();
	}
}
