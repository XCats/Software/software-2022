package org.xcats.frc.lib.devices.actuation.speedcontrol.impl;

import java.util.Arrays;
import java.util.List;

import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedControllerGroup;
import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedControllerStatus;
import org.xcats.frc.lib.logging.XLogString;

@Deprecated
public class XCANTalonSRXSpeedControlGroup implements XSpeedControllerGroup<XCANTalonSRX> {

	private final String mName;
	private final XCANTalonSRX mMaster;
	private final List<XCANTalonSRX> mFollowers;

	public XCANTalonSRXSpeedControlGroup(String name, XCANTalonSRX master, XCANTalonSRX... followers) {
		this.mName = name;
		this.mMaster = master;
		this.mFollowers = Arrays.asList(followers);
	}

	@Override
	public List<XCANTalonSRX> getFollowers() {
		return this.mFollowers;
	}

	@Override
	public XCANTalonSRX getMaster() {
		return this.mMaster;
	}

	@Override
	public void setInverted() {
		this.mMaster.setInverted();
	}

	@Override
	public boolean isInverted() {
		return this.mMaster.isInverted();
	}

	@Override
	public void stop() {
		this.alterSetPoint(0);
	}

	@Override
	public String getName() {
		return this.mName;
	}

	@Override
	public void alterSetPoint(double newSetPoint) {
		this.mMaster.alterSetPoint(newSetPoint);
	}

	@Override
	public double getSetPoint() {
		return this.mMaster.getSetPoint();
	}

	@Override
	public XSpeedControllerStatus getStatus() {
		return null;
	}

	@Override
	public void initialize() {
		mMaster.initialize();
		this.mFollowers.stream().forEach(f -> f.initialize());
		this.mFollowers.stream().forEach(f -> f.getMotor().follow(this.mMaster.getMotor()));
	}

	@Override
	public XLogString getLogString() {
		return null;
	}
}
