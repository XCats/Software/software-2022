package org.xcats.frc.lib.devices.sensors.impl;

import org.xcats.frc.lib.devices.sensors.XEncoder;

import edu.wpi.first.util.sendable.Sendable;
import edu.wpi.first.util.sendable.SendableBuilder;

//class takes in two motor encoders and returns their average for each value
public class XEncoderGroup implements XEncoder {
    
    private XEncoder encoder1;
    private XEncoder encoder2;
    private final String name;
    private XEncoderSendable mSendable;
    private final Units mUnitsText;

    public XEncoderGroup(String name, XEncoder encoder1, XEncoder encoder2)
    {
        this.encoder1 = encoder1;
        this.encoder2 = encoder2;
        this.name = name;

        if(this.encoder1.getDistanceUnit() != this.encoder2.getDistanceUnit()) {
            throw new IllegalArgumentException("Encoder unit mismatch");
        }

        this.mUnitsText = this.encoder1.getDistanceUnit();
    }

    @Override
    public double getRawDistanceTicks() {
        return (encoder1.getRawDistanceTicks()+encoder2.getRawDistanceTicks())/2;
    }

    @Override
    public double getRawVelocityTicks() {
        return (encoder1.getRawVelocityTicks()+encoder2.getRawVelocityTicks())/2;
    }

    @Override
    public double getRotationalDistance() {
        return (encoder1.getRotationalDistance()+encoder2.getRotationalDistance())/2;
    }

    @Override
    public double getRotationalVelocity() {
        return (encoder1.getRotationalVelocity()+encoder2.getRotationalVelocity())/2;
    }

    @Override
    public double getRPM() {
        return (encoder1.getRPM()+encoder2.getRPM())/2;
    }

    @Override
    public double getLinearDistance() {
        return (encoder1.getLinearDistance()+encoder2.getLinearDistance())/2;
    }

    @Override
    public double getLinearVelocity() {
        return (encoder1.getLinearVelocity()+encoder2.getLinearVelocity())/2;
    }

    @Override
    public void zero() {
        encoder1.zero();
        encoder2.zero();
    }

    @Override
    public double convertToTicksFromDistance(double distance) {
        return (encoder1.convertToTicksFromDistance(distance)+encoder2.convertToTicksFromDistance(distance))/2;
    }

    @Override
    public Units getDistanceUnit() {
        return encoder1.getDistanceUnit();
    }

    @Override
	public Sendable getSendable() {
		if (mSendable == null) {
			mSendable = new XEncoderSendable();
		}

		return mSendable;
	}

	private class XEncoderSendable implements Sendable {

		@Override
		public void initSendable(SendableBuilder builder) {
			builder.addStringProperty("Name", () -> name, null);
			builder.addDoubleProperty("Raw Distance", XEncoderGroup.this::getRawDistanceTicks, null);
			builder.addDoubleProperty("Raw Velocity", XEncoderGroup.this::getRawVelocityTicks, null);
			builder.addDoubleProperty("Rotations", XEncoderGroup.this::getRotationalDistance, null);
			builder.addDoubleProperty("Rotations per second", XEncoderGroup.this::getRotationalVelocity, null);
			builder.addDoubleProperty("Distance (" + mUnitsText + ")", XEncoderGroup.this::getLinearDistance, null);
			builder.addDoubleProperty("Velocity (" + mUnitsText + " per second)", XEncoderGroup.this::getLinearVelocity, null);
			builder.addDoubleProperty("Velocity (RPM)", XEncoderGroup.this::getRPM, null);
		}
	}



}
