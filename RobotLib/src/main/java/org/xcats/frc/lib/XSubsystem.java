package org.xcats.frc.lib;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public abstract class XSubsystem extends SubsystemBase {
    private NetworkTable mNetworkTable;

    public XSubsystem() {
        super();
        mNetworkTable = NetworkTableInstance.getDefault().getTable("RobotData").getSubTable(this.getName());
    }

    /**
     * Defaults to true, can be overridden to return false or conditionally if needed.
     * @return Whether or not we want to log the subsystem info to the network table. 
     */
    protected boolean logToNetworkTable() {
        return true;
    }

    protected NetworkTable getSubsystemNetworkTable() {
        return mNetworkTable;
    }

    public void periodic() {
        if(logToNetworkTable()) putNetworkTableInfo();
    }

    /**
     * Any network table puts should be placed in here.
     */
    protected abstract void putNetworkTableInfo();

    /**
     * This method should set the subsystem to a motionless, stopped state
     */
    protected abstract void stop();

}
