package org.xcats.frc.lib.devices.sensors;

public interface XLimitSwitch {

	boolean getCurrentValue();

	String getName();

}
