package org.xcats.frc.lib.devices.sensors.impl;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonFXConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

//Ref: https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/tree/master/Java%20Talon%20FX%20(Falcon%20500)/IntegratedSensor

public class XTalonFXEncoder extends XBaseEncoder {
	private static final String ENCODER_TYPE = "TalonFX";
	private static final double ENCODER_TPR = 2048; // Encoder ticks per revolution

	private final WPI_TalonFX mTalon;

	public XTalonFXEncoder(String name, WPI_TalonFX talon, double distancePerRotation, Units distanceUnit) {
		super(name, ENCODER_TYPE, ENCODER_TPR, ENCODER_TPR / 10, distancePerRotation, distanceUnit);
		this.mTalon = talon;

		TalonFXConfiguration configs = new TalonFXConfiguration();
		configs.primaryPID.selectedFeedbackSensor = FeedbackDevice.IntegratedSensor;
		this.mTalon.configAllSettings(configs);
	}

	@Override
	public void zero() {
		this.mTalon.getSensorCollection().setIntegratedSensorPosition(0, 0);
	}

	@Override
	public double getRawDistanceTicks() {
		return mTalon.getSelectedSensorPosition();
	}

	@Override
	public double getRawVelocityTicks() {
		return mTalon.getSelectedSensorVelocity();
	}
}
