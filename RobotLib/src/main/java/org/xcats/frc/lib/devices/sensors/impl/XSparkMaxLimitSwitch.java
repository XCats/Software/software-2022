package org.xcats.frc.lib.devices.sensors.impl;

import com.revrobotics.CANSparkMax;
import com.revrobotics.SparkMaxLimitSwitch;
import org.xcats.frc.lib.devices.sensors.XLimitSwitch;

public class XSparkMaxLimitSwitch implements XLimitSwitch {
	private final String mName;
	private final CANSparkMax mSparkMax;
	private final boolean mIsFwdSwitch;
	private final SparkMaxLimitSwitch.Type mSwitchPolarity;

	public XSparkMaxLimitSwitch(String name, CANSparkMax sparkMax, boolean isFwdSwitch, boolean switchPolarity) {
		mName = name;
		mSparkMax = sparkMax;
		mIsFwdSwitch = isFwdSwitch;
		mSwitchPolarity = switchPolarity ? SparkMaxLimitSwitch.Type.kNormallyClosed : SparkMaxLimitSwitch.Type.kNormallyOpen;
	}

	@Override
	public boolean getCurrentValue() {
		return mIsFwdSwitch ? mSparkMax.getForwardLimitSwitch(mSwitchPolarity).isPressed()
				: mSparkMax.getReverseLimitSwitch(mSwitchPolarity).isPressed();
	}

	@Override
	public String getName() {
		return mName;
	}
}
