package org.xcats.frc.lib.devices.sensors;

public interface XPotentiometer {

	/**
	 * Gets the "raw" value of the sensor, typically either in ticks or volts
	 * 
	 * @return The raw value
	 */
	double getRawValue();

	/**
	 * Returns the angle the pot is currently at, based on the predefined limits.
	 * 
	 * @return The angle, in degrees
	 */
	double getAngle();

	/**
	 * Converts the angle to native units, if necessary for advanced controls.
	 * 
	 * @param angle The angle, in degrees
	 * @return The native sensor units that represent this angle
	 */
	double getTicksFromAngle(double angle);

}
