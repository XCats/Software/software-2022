package org.xcats.frc.lib;

import frc.robot.Robot;

/**
 * An interface implemented by any class that should be initialized when {@link Robot#robotInit()} is run, and stopped
 * when the robot is disabled (when {@link Robot#disabledInit()} is run).
 */
public interface XInitializable {
	/**
	 * Runs when {@link Robot#robotInit()} is run
	 */
	void initialize();

	/**
	 * Stops the object
	 */
	void stop();
}
