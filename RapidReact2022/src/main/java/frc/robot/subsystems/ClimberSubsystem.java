package frc.robot.subsystems;

import org.xcats.frc.lib.XSubsystem;
import org.xcats.frc.lib.devices.sensors.impl.XEncoderGroup;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.robot.Constants;

public class ClimberSubsystem extends XSubsystem {

    //Create Function that turns the motors ON and Off

    MotorController climberMotor;
    XEncoderGroup climberEncoderGroup;
    DoubleSolenoid brake;

    public ClimberSubsystem(MotorController climberMotor, XEncoderGroup climberEncoderGroup, 
        DoubleSolenoid brake)
    {
        this.climberMotor = climberMotor;
        this.climberEncoderGroup = climberEncoderGroup;
        this.brake = brake;
    }

    public void climberArmsUp()
    {
        this.climberMotor.set(Constants.CLIMB_UP_SPEED);
    }

    public void climberArmsDown()
    {
        this.climberMotor.set(Constants.CLIMB_DOWN_SPEED);
    }

    public void turnOffMotors()
    {
        this.climberMotor.set(0);
    }

    public void brake(){
        this.brake.set(Value.kReverse);
    }

    public void release(){
        this.brake.set(Value.kForward);
    }

    public boolean isArmsPositionLimitReached(){
        return this.getArmsPosition() >= Constants.CLIMBER_ARMS_LOWER_POSITION_LIMIT;
    }

    public double getArmsPosition(){
        return this.climberEncoderGroup.getLinearDistance();
    }

    public void zeroEncoders(){
        this.climberEncoderGroup.zero();
    }

    @Override
    protected void putNetworkTableInfo() {
        super.getSubsystemNetworkTable().getEntry("climberMotorSpeed").setDouble(this.climberMotor.get());
        super.getSubsystemNetworkTable().getEntry("climberArmsLinearDistance").setDouble(this.getArmsPosition());
    }

    @Override
    protected void stop() {
        turnOffMotors();
    }
    
}
