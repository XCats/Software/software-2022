package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;

import org.xcats.frc.lib.XSubsystem;
import org.xcats.frc.lib.devices.sensors.XEncoder;
import org.xcats.frc.lib.devices.sensors.impl.XEncoderGroup;

//Subsystem controlling the driving, also estimates robot position
public class DriveBaseSubsystem extends XSubsystem {
    
    private CANSparkMax driveLeft;
    private CANSparkMax driveRight;
    private XEncoder leftEncoder;
    private XEncoder rightEncoder;
    private XEncoder driveEncoder;

    public DriveBaseSubsystem(CANSparkMax driveLeft, CANSparkMax driveRight, XEncoder leftEncoder, XEncoder rightEncoder)
    {
        this.driveLeft = driveLeft;
        this.driveRight = driveRight;

        this.leftEncoder = leftEncoder;
        this.rightEncoder = rightEncoder;
        this.driveEncoder = new XEncoderGroup("driveEncoder", leftEncoder, rightEncoder);
    }

    public void setLeft(double speed) //sets left wheel speed
    {
        this.driveLeft.set(speed);
    }
    
    public void setRight(double speed) //sets right wheel speed
    {
        this.driveRight.set(speed);
    }

    public double getAverageDistance()
    {
        return driveEncoder.getLinearDistance();
    }

    public double getLeftDistance()
    {
        return leftEncoder.getLinearDistance();
    }

    public double getRightDistance()
    {
        return rightEncoder.getLinearDistance();
    }

    public void zeroEncoders() {
        leftEncoder.zero();
        rightEncoder.zero();
    }

    public void off()
    {
        this.driveLeft.set(0);
        this.driveRight.set(0);
    }

    @Override
    protected void stop()
    {
        off();
    }

    @Override
    protected void putNetworkTableInfo()
    {
        super.getSubsystemNetworkTable().getEntry("leftWheelSpeed").setDouble(driveLeft.get());
        super.getSubsystemNetworkTable().getEntry("rightWheelSpeed").setDouble(driveRight.get());
    }
}