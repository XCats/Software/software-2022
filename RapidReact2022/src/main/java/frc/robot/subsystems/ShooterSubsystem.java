package frc.robot.subsystems;

import org.xcats.frc.lib.XSubsystem;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.robot.Constants;

//class controlling the shooter flywheels

public class ShooterSubsystem extends XSubsystem {

    MotorController flywheelOneMotor;
    
    public ShooterSubsystem(MotorController flywheelOneMotor )
    {
        this.flywheelOneMotor = flywheelOneMotor;
    }

    public void shootersOn() //turns on flywheels
    {
        this.flywheelOneMotor.set(Constants.SHOOTER_FLYWHEEL_SPEED);
    }

    public void shootersOff() //turns off flywheels
    {
        this.flywheelOneMotor.set(0);
    }

    @Override
    protected void stop()
    {
        this.flywheelOneMotor.set(0);
    }

    @Override
    protected void putNetworkTableInfo()
    {
        super.getSubsystemNetworkTable().getEntry("shooterSpeed").setDouble(flywheelOneMotor.get());
    }
}