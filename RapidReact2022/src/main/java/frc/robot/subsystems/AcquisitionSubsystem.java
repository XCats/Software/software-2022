package frc.robot.subsystems;

import org.xcats.frc.lib.XSubsystem;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.robot.Constants;

//subsytem controlling the acquisition

public class AcquisitionSubsystem extends XSubsystem
{
    private MotorController acquisitionRollers;
    private DoubleSolenoid acquisitionArm;

    public AcquisitionSubsystem(MotorController acquisitionRollers, DoubleSolenoid acquisitionArm)
    {
        this.acquisitionRollers = acquisitionRollers;
        this.acquisitionRollers.setInverted(false);

        this.acquisitionArm = acquisitionArm;
    }

    public void armUp() 
    {
        this.acquisitionArm.set(Value.kForward);
    }

    public void armDown()
    {
        this.acquisitionArm.set(Value.kReverse);
    }

    public void rollersOn() //turns on rollers when called
    {
        this.acquisitionRollers.set(Constants.ACQUISITION_ROLLER_SPEED);
    }

    public void rollersOff() //turns off rollers when called
    {
        this.acquisitionRollers.set(0);
    }

    @Override
    protected void stop()
    {
        this.acquisitionRollers.set(0);
    }

    @Override
    protected void putNetworkTableInfo()
    {
        super.getSubsystemNetworkTable().getEntry("rollerSpeed").setDouble(acquisitionRollers.get());
    }
}