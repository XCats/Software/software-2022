package frc.robot.subsystems;

import org.xcats.frc.lib.XSubsystem;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.robot.Constants;

//Subsystem Controlling The Conveyor Motor
public class ConveyorSubsystem extends XSubsystem
{
    MotorController m_conveyorMotor;
    DoubleSolenoid cargoDoor;

    //Defines m_conveyorMotor
    public ConveyorSubsystem(MotorController conveyorMotor, DoubleSolenoid cargoDoor)
    {
        m_conveyorMotor = conveyorMotor;
        this.cargoDoor = cargoDoor;
    }

    public void cargoDoorOpen()
    {
        this.cargoDoor.set(Value.kForward);
    }

    public void cargoDoorClose()
    {
        this.cargoDoor.set(Value.kReverse);
    }

    //Turns on motor/sets motor speed to pre-defined constant
    public void TurnOnMotor()
    {
        m_conveyorMotor.set(Constants.CONVEYOR_SPEED);
    }

    public void TurnOffMotor()
    {
        m_conveyorMotor.set(0);
    }

    @Override
    protected void putNetworkTableInfo() 
    {
        super.getSubsystemNetworkTable().getEntry("conveyorSpeed").setDouble(m_conveyorMotor.get());
    }

    @Override
    protected void stop() 
    {
        
    }
}