package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.AcquisitionSubsystem;

public class AcquisitionArmUpCommand extends CommandBase {

    AcquisitionSubsystem acquisitionSubsystem;

    public AcquisitionArmUpCommand(AcquisitionSubsystem acquisitionSubsystem)
    {
        this.acquisitionSubsystem = acquisitionSubsystem;
 
        addRequirements(acquisitionSubsystem);
    }
 
    @Override
    public void initialize()
     {
 
     }
 
     @Override
     public void execute()
     {
         this.acquisitionSubsystem.armUp();
     }
 
     @Override
     public void end(boolean interupted)
     {
 
     }
 
     @Override
     public boolean isFinished()
     {
         return true;
     }   
 }
