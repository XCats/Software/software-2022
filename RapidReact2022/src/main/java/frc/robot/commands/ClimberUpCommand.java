package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberUpCommand extends SequentialCommandGroup {
    
    public ClimberUpCommand(ClimberSubsystem climberSubsystem, double transitionDelay) {
        super(new ClimberBrakeOffCommand(climberSubsystem),
            new ClimberArmsDownCommand(climberSubsystem),
            new WaitCommand(transitionDelay),
            new ClimberArmsUpCommand(climberSubsystem));
    }
}
