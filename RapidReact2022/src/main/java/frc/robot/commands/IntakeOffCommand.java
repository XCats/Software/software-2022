package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.AcquisitionSubsystem;
import frc.robot.subsystems.ConveyorSubsystem;

public class IntakeOffCommand extends SequentialCommandGroup 
{
    public IntakeOffCommand(AcquisitionSubsystem acquisitionSubsystem, ConveyorSubsystem conveyorSubsystem)
    {
        super.addCommands(new AcquisitionArmUpCommand(acquisitionSubsystem),
            new ParallelCommandGroup( new AcquisitionRollersOffCommand(acquisitionSubsystem), 
                new ConveyorOffCommand(conveyorSubsystem))); 
    }
}
