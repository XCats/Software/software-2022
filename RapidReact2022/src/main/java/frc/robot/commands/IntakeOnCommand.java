package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.AcquisitionSubsystem;
import frc.robot.subsystems.ConveyorSubsystem;

public class IntakeOnCommand extends SequentialCommandGroup 
{
    public IntakeOnCommand(AcquisitionSubsystem acquisitionSubsystem, ConveyorSubsystem conveyorSubsystem)
    {
       super.addCommands(new AcquisitionArmDownCommand(acquisitionSubsystem),
            new ParallelCommandGroup(new AcquisitionRollersOnCommand(acquisitionSubsystem), 
                new ConveyorOnCommand(conveyorSubsystem))); 
    }  
}