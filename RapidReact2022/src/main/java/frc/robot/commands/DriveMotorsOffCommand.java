package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveBaseSubsystem;

public class DriveMotorsOffCommand extends CommandBase {

    DriveBaseSubsystem driveBaseSubsystem; 

    public DriveMotorsOffCommand(DriveBaseSubsystem driveBaseSubsystem)
    {
        this.driveBaseSubsystem = driveBaseSubsystem;

        this.addRequirements(driveBaseSubsystem);
    }

    @Override
    public void initialize()
    {
        
    }

    @Override
    public void execute()
    {
        this.driveBaseSubsystem.off();
    }

    @Override
    public void end(boolean interrupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }
}
