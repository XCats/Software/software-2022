package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.AcquisitionSubsystem;

//Command will turn the acquisition rollers on and then stop

public class AcquisitionRollersOnCommand extends CommandBase
{
    
    AcquisitionSubsystem acquisitionSubsystem;

    public AcquisitionRollersOnCommand(AcquisitionSubsystem acquisitionSubsystem)
    {
        this.acquisitionSubsystem = acquisitionSubsystem;

        addRequirements(acquisitionSubsystem);
    }

    @Override
    public void initialize()
    {

    }
    
    @Override
    public void execute()
    {
        this.acquisitionSubsystem.rollersOn();
    }

    @Override
    public void end(boolean interrupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }

}