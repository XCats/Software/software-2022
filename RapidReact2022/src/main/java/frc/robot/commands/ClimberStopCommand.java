package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberStopCommand extends SequentialCommandGroup {
    
    public ClimberStopCommand(ClimberSubsystem climberSubsystem)
    {
        super(new ClimberArmsOffCommand(climberSubsystem), 
            new ClimberBrakeOnCommand(climberSubsystem));
    }
}
