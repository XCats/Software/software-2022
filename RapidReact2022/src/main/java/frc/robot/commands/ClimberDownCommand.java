package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ConditionalCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberDownCommand extends ConditionalCommand {
    
    public ClimberDownCommand(ClimberSubsystem climberSubsystem){
        super(new ClimberStopCommand(climberSubsystem), 
            new SequentialCommandGroup(new ClimberArmsDownCommand(climberSubsystem),
                new ClimberArmsAtLowerLimitCommand(climberSubsystem),
                new ClimberArmsOffCommand(climberSubsystem)),
                climberSubsystem::isArmsPositionLimitReached);
            
    }
}
