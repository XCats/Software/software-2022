package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ShooterSubsystem;

//command will turn on the flywheels and then stop

public class ShooterOnCommand extends CommandBase {
    
    ShooterSubsystem shooterSubsystem;

    public ShooterOnCommand(ShooterSubsystem shooterSubsystem)
    {
        this.shooterSubsystem = shooterSubsystem;
    }

    @Override
    public void initialize()
    {

    }

    @Override
    public void execute()
    {
        this.shooterSubsystem.shootersOn();
    }

    @Override
    public void end(boolean interrupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }
}