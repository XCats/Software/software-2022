package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ConveyorSubsystem;

public class CargoDoorCloseCommand extends CommandBase {

    ConveyorSubsystem conveyorSubsystem;

    public CargoDoorCloseCommand(ConveyorSubsystem conveyorSubsystem)
    {
        this.conveyorSubsystem = conveyorSubsystem;
        addRequirements(conveyorSubsystem);
    }
    
    @Override
    public void initialize()
    {

    }

    @Override
    public void execute()
    {
        this.conveyorSubsystem.cargoDoorClose();
    }

    @Override
    public void end(boolean interupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }   
}
