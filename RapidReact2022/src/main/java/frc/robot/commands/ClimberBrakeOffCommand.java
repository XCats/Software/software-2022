package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberBrakeOffCommand extends CommandBase {

    ClimberSubsystem climberSubsystem;

    public ClimberBrakeOffCommand(ClimberSubsystem climberSubsystem){
        this.climberSubsystem = climberSubsystem;
    }

    @Override
    public void initialize() {}
    
    
    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        this.climberSubsystem.release();
    }
    
    
    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}
    
    
    // Returns true when the command should end.
    @Override
    public boolean isFinished()
    {
        return true;
    }
}