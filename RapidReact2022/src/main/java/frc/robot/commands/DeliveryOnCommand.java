package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.ConveyorSubsystem;

//command will create a shooterOnCommand and a conveyorOnCommand to turn on the shooting system
public class DeliveryOnCommand extends SequentialCommandGroup {

    public DeliveryOnCommand(ShooterSubsystem shooterSubsystem, ConveyorSubsystem conveyorSubsystem)
    {
        super(new CargoDoorOpenCommand(conveyorSubsystem),
            new ParallelCommandGroup(new ShooterOnCommand(shooterSubsystem), 
                new ConveyorOnCommand(conveyorSubsystem)));
    }
}
