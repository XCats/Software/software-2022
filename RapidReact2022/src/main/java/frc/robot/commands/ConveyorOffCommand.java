package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ConveyorSubsystem;

//Turns Conveyor Off
public class ConveyorOffCommand extends CommandBase 
{
    
    ConveyorSubsystem m_conveyorSubsystem;

    public ConveyorOffCommand(ConveyorSubsystem conveyorSubsystem)
    {
        m_conveyorSubsystem = conveyorSubsystem;
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {}
    
    
    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() 
    {
        m_conveyorSubsystem.TurnOffMotor();
    }
    
    
    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}
    
    
    // Returns true when the command should end.
    @Override
    public boolean isFinished()
    {
        return true;
    }
}
