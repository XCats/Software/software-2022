package frc.robot.commands;

import frc.robot.subsystems.DriveBaseSubsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;

//command will move the robot X inches and then turn off the drive

public class MoveXInchesCommand extends CommandBase {

    double inches;
    double speed;
    DriveBaseSubsystem driveBaseSubsystem;

    public MoveXInchesCommand(DriveBaseSubsystem driveBaseSubsystem, double inches, double speed)
    {
        this.driveBaseSubsystem = driveBaseSubsystem;
        this.inches = inches;
        this.speed = speed;
        addRequirements(driveBaseSubsystem);
    }

    
    @Override
    public void initialize()
    {
        this.driveBaseSubsystem.zeroEncoders();
    }

    @Override
    public void execute()
    {
        if (Math.abs(driveBaseSubsystem.getAverageDistance()) < inches-6)
        {
            this.driveBaseSubsystem.setRight(speed);
            this.driveBaseSubsystem.setLeft(speed);
        }
        else
        {
            this.driveBaseSubsystem.setRight(speed/10);
            this.driveBaseSubsystem.setLeft(speed/10);
        }
    }

    @Override
    public void end(boolean interrupted)
    {
        this.driveBaseSubsystem.off();
    }

    @Override
    public boolean isFinished()
    {
        return Math.abs(driveBaseSubsystem.getAverageDistance()) > inches;
    }
}
