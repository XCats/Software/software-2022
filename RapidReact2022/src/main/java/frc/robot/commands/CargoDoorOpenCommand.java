package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ConveyorSubsystem;

public class CargoDoorOpenCommand extends CommandBase {

    ConveyorSubsystem conveyorSubsystem;

    public CargoDoorOpenCommand(ConveyorSubsystem conveyorSubsystem)
    {
        this.conveyorSubsystem = conveyorSubsystem;
        addRequirements(conveyorSubsystem);
    }
    
    @Override
    public void initialize()
    {

    }

    @Override
    public void execute()
    {
        this.conveyorSubsystem.cargoDoorOpen();
    }

    @Override
    public void end(boolean interupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }   
}
