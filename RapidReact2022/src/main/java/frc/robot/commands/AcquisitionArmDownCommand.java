package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.AcquisitionSubsystem;

public class AcquisitionArmDownCommand extends CommandBase {

	AcquisitionSubsystem acquisitionSubsystem;


	public AcquisitionArmDownCommand(AcquisitionSubsystem acquisitionSubsystem) {
		this.acquisitionSubsystem = acquisitionSubsystem;

		addRequirements(acquisitionSubsystem);
	}

	@Override
	public void initialize() {

	}

	@Override
	public void execute() {
		this.acquisitionSubsystem.armDown();
	}

	@Override
	public void end(boolean interupted) {

	}

	@Override
	public boolean isFinished() {
		return true;
	}
}
