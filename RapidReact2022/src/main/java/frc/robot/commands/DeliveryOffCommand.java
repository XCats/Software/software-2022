package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.ConveyorSubsystem;

//command will create a shooterOffCommand and a conveyorOffCommand to turn off the shooting system
public class DeliveryOffCommand extends SequentialCommandGroup {

    public DeliveryOffCommand(ShooterSubsystem shooterSubsystem, ConveyorSubsystem conveyorSubsystem)
    {
        super(new CargoDoorCloseCommand(conveyorSubsystem),
            new ParallelCommandGroup(new ShooterOffCommand(shooterSubsystem), 
                new ConveyorOffCommand(conveyorSubsystem)));
    }
}