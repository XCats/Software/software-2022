package frc.robot.commands;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class ToggleCameraCommand extends CommandBase {

	NetworkTableEntry cameraEntry;

	public ToggleCameraCommand() {
		NetworkTableInstance inst = NetworkTableInstance.getDefault();
		NetworkTable table = inst.getTable("");

		cameraEntry = table.getEntry("backupCameraOn");

		if(cameraEntry.getNumber(100).intValue() == 100) cameraEntry.setNumber(0);
	}

	@Override
	public void execute()
	{
		int currentValue = cameraEntry.getNumber(0).intValue();
		int futureValue = 0;
		if (currentValue == 0) futureValue = 1;
		cameraEntry.setNumber(futureValue);
	}

	@Override
	public void end(boolean interupted)
	{

	}

	@Override
	public boolean isFinished()
	{
		return true;
	}
}