package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj.Joystick;
import frc.robot.subsystems.DriveBaseSubsystem;
import frc.robot.Constants;

public class DefaultDriveCommand extends CommandBase {

    DriveBaseSubsystem driveBaseSubsystem;
    Joystick driverLeftStick;
    Joystick driverRightStick;

    public DefaultDriveCommand(DriveBaseSubsystem driveBaseSubsystem, Joystick driverLeftStick, Joystick driverRightStick)
    {
        this.driveBaseSubsystem = driveBaseSubsystem;
        this.driverLeftStick = driverLeftStick;
        this.driverRightStick = driverRightStick;

        addRequirements(driveBaseSubsystem);
    }

    @Override
    public void initialize()
    {
        
    }

    @Override
    public void execute()
    {
        this.driveBaseSubsystem.setLeft(-driverLeftStick.getRawAxis(Constants.DRIVE_JOYSTICK_Y_AXIS));
        this.driveBaseSubsystem.setRight(-driverRightStick.getRawAxis(Constants.DRIVE_JOYSTICK_Y_AXIS));
    }

    @Override
    public void end(boolean interupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return false;
    }
}
