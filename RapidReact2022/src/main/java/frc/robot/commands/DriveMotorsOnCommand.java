package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveBaseSubsystem;

public class DriveMotorsOnCommand extends CommandBase {

    DriveBaseSubsystem driveBaseSubsystem; 
    boolean isForward;

    public DriveMotorsOnCommand(DriveBaseSubsystem driveBaseSubsystem, boolean isForward)
    {
        this.driveBaseSubsystem = driveBaseSubsystem;
        this.isForward = isForward;

        this.addRequirements(driveBaseSubsystem);
    }

    @Override
    public void initialize()
    {

    }

    @Override
    public void execute()
    {
        if(this.isForward)
        {
            this.driveBaseSubsystem.setLeft(.5);
            this.driveBaseSubsystem.setRight(.5);
        }
        else
        {
            this.driveBaseSubsystem.setLeft(-.5);
            this.driveBaseSubsystem.setRight(-.5);  
        }
    }

    @Override
    public void end(boolean interrupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }
}
