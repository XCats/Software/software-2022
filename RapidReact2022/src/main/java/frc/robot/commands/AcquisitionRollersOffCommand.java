package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.AcquisitionSubsystem;

//command will turn the acquisition rollers off and then stop

public class AcquisitionRollersOffCommand extends CommandBase{

    AcquisitionSubsystem acquisitionSubsystem;

    public AcquisitionRollersOffCommand(AcquisitionSubsystem acquisitionSubsystem)
    {
        this.acquisitionSubsystem = acquisitionSubsystem;

        addRequirements(acquisitionSubsystem);
    }

    @Override
    public void initialize()
    {

    }

    @Override
    public void execute()
    {
        this.acquisitionSubsystem.rollersOff();
    }

    @Override
    public void end(boolean interupted)
    {

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }
}
