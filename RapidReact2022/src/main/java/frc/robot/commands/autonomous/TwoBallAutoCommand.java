package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.IntakeOffCommand;
import frc.robot.commands.IntakeOnCommand;
import frc.robot.commands.MoveXInchesCommand;
import frc.robot.subsystems.AcquisitionSubsystem;
import frc.robot.subsystems.ConveyorSubsystem;
import frc.robot.subsystems.DriveBaseSubsystem;
import frc.robot.subsystems.ShooterSubsystem;

public class TwoBallAutoCommand extends SequentialCommandGroup {

    public TwoBallAutoCommand(DriveBaseSubsystem driveBaseSubsystem,
                              ShooterSubsystem shooterSubsystem,
                              ConveyorSubsystem conveyorSubsystem,
                              AcquisitionSubsystem acquisitionSubsystem,
                              double distance,
                              double speed,
                              double acquireTime,
                              double shootTime,
                              double reverseDistance)
    {
        super(
            new ShootAutoCommand(shooterSubsystem, conveyorSubsystem, shootTime),
            new IntakeOnCommand(acquisitionSubsystem, conveyorSubsystem),
            new MoveXInchesCommand(driveBaseSubsystem, distance, speed),
            new WaitCommand(acquireTime),
            new IntakeOffCommand(acquisitionSubsystem, conveyorSubsystem),
            new MoveXInchesCommand(driveBaseSubsystem, reverseDistance, -speed),
            new ShootAutoCommand(shooterSubsystem, conveyorSubsystem, shootTime)
        );
    }
}
