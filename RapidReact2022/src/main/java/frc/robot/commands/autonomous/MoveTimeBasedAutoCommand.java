package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.DriveMotorsOffCommand;
import frc.robot.commands.DriveMotorsOnCommand;
import frc.robot.subsystems.DriveBaseSubsystem;

//command will move the robot for an amount of time
public class MoveTimeBasedAutoCommand extends SequentialCommandGroup {
        public MoveTimeBasedAutoCommand(DriveBaseSubsystem driveSubsystem,
        double driveWaitTime)
    {
        super
        (
            new DriveMotorsOnCommand(driveSubsystem, true), //start moving
            new WaitCommand(driveWaitTime), //move
            new DriveMotorsOffCommand(driveSubsystem) //stop moving
        );
    }   
}
