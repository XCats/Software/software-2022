package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.DriveBaseSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.ConveyorSubsystem;

public class TimedAutoModeCommand extends SequentialCommandGroup {
    
    //turn on shoot motors for - seconds, then turn them off
    //drive backwards for - seconds
    //then turn off drive motors

    public TimedAutoModeCommand(DriveBaseSubsystem driveSubsystem, 
        ShooterSubsystem shooterSubsystem, 
        ConveyorSubsystem conveyorSubsystem,
        double shootWaitTime, 
        double driveWaitTime)
    {
        super
        (
            new ShootAutoCommand(shooterSubsystem, conveyorSubsystem, shootWaitTime), //shoot ball
            new MoveTimeBasedAutoCommand(driveSubsystem, driveWaitTime) //move
        );
    }   
}
