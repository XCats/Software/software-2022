package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.MoveXInchesCommand;
import frc.robot.subsystems.ConveyorSubsystem;
import frc.robot.subsystems.DriveBaseSubsystem;
import frc.robot.subsystems.ShooterSubsystem;

//autonomous mode sequential command that will shoot the ball and then move out of the tarmac
public class ShootAndMoveAutoCommand extends SequentialCommandGroup {

    public ShootAndMoveAutoCommand(DriveBaseSubsystem driveBaseSubsystem, 
        ShooterSubsystem shooterSubsystem,
        ConveyorSubsystem conveyorSubsystem,
        double distance,
        double speed,
        double shootTime)
    {     
        super(new ShootAutoCommand(shooterSubsystem, conveyorSubsystem, shootTime), //shoot ball
            new MoveXInchesCommand(driveBaseSubsystem, distance, speed)); //drive out of tarmac
    }
}
