package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.commands.MoveXInchesCommand;
import frc.robot.subsystems.AcquisitionSubsystem;
import frc.robot.subsystems.ConveyorSubsystem;
import frc.robot.subsystems.DriveBaseSubsystem;
import frc.robot.subsystems.ShooterSubsystem;

//Adds a widget to smart dash board to allow selection of auto mode
public class AutoCommandSelector {

    private final SendableChooser<Command> mAutoChooser = new SendableChooser<Command>();

    public AutoCommandSelector(ShooterSubsystem shooterSubsystem, 
        ConveyorSubsystem conveyorSubsystem,
        DriveBaseSubsystem driveBaseSubsystem,
        AcquisitionSubsystem acquisitionSubsystem) {

        mAutoChooser.setDefaultOption("Two Ball (Distance Based)",
                new TwoBallAutoCommand(driveBaseSubsystem, shooterSubsystem, conveyorSubsystem, acquisitionSubsystem,
                        Constants.AUTONOMOUS_DISTANCE_TO_BALL, Constants.AUTONOMOUS_BACKUP_SPEED,
                        Constants.AUTONOMOUS_ACQUISITION_TIME, Constants.AUTONOMOUS_SHOOT_TIME, Constants.AUTONOMOUS_TO_HUB_DISTANCE));
        mAutoChooser.addOption("Shoot&Move (Distance Based)",
            new ShootAndMoveAutoCommand(driveBaseSubsystem, shooterSubsystem, conveyorSubsystem, 
                Constants.AUTONOMOUS_BACKUP_DISTANCE, Constants.AUTONOMOUS_BACKUP_SPEED, Constants.AUTONOMOUS_SHOOT_TIME));
        mAutoChooser.addOption("Shoot&Move (Time Based)", 
            new TimedAutoModeCommand(driveBaseSubsystem, shooterSubsystem, conveyorSubsystem,
                Constants.AUTONOMOUS_SHOOT_TIME, Constants.AUTONOMOUS_BACKUP_TIME));
        mAutoChooser.addOption("Shoot Only", 
            new ShootAutoCommand(shooterSubsystem, conveyorSubsystem, Constants.AUTONOMOUS_SHOOT_TIME));
        mAutoChooser.addOption("Move Only (Distance Based)",
            new MoveXInchesCommand(driveBaseSubsystem, Constants.AUTONOMOUS_BACKUP_DISTANCE, Constants.AUTONOMOUS_BACKUP_SPEED));
        mAutoChooser.addOption("Move Only (Time Based)",
            new MoveTimeBasedAutoCommand(driveBaseSubsystem, Constants.AUTONOMOUS_BACKUP_TIME));
        mAutoChooser.addOption("Do nothing", null);

        SmartDashboard.putData("Auto Mode", mAutoChooser);
    }

    public Command getSelectedCommand() {
        return mAutoChooser.getSelected();
    }
}