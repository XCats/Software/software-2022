package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.DeliveryOffCommand;
import frc.robot.commands.DeliveryOnCommand;
import frc.robot.subsystems.ConveyorSubsystem;
import frc.robot.subsystems.ShooterSubsystem;

//autonomous mode sequential command that will shoot the ball
public class ShootAutoCommand extends SequentialCommandGroup {

    public ShootAutoCommand(
        ShooterSubsystem shooterSubsystem,
        ConveyorSubsystem conveyorSubsystem,
        double shootTime)
    {     
        super(new DeliveryOnCommand(shooterSubsystem, conveyorSubsystem), //shoot on
            new WaitCommand(shootTime), //shooting
            new DeliveryOffCommand(shooterSubsystem, conveyorSubsystem)); //shoot off
    }
}