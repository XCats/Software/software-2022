// Copyright (c) FIRST and other WPILib contributors.

// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.lang.Math;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    
    //Motor speed constants
    public static final double ACQUISITION_ROLLER_SPEED = 0.8;
    public static final double SHOOTER_FLYWHEEL_SPEED = 0.9;
    public static final double CONVEYOR_SPEED = 0.5;
    public static final double CLIMB_UP_SPEED = -0.40;
    public static final double CLIMB_DOWN_SPEED = 0.5;

    //Autonomous
    public static final double AUTONOMOUS_BACKUP_TIME = 2;
    public static final double AUTONOMOUS_BACKUP_DISTANCE = 115;
    public static final double AUTONOMOUS_BACKUP_SPEED = 0.25;
    public static final double AUTONOMOUS_SHOOT_TIME = 1.0;
    public static final double AUTONOMOUS_DRIVE_TIME = 1.5;

    public static final double AUTONOMOUS_DISTANCE_TO_BALL = 120;
    public static final double AUTONOMOUS_ACQUISITION_TIME = 0.5;
    public static final double AUTONOMOUS_DOUBLE_SHOOT_TIME = 3;
    public static final double AUTONOMOUS_TO_HUB_DISTANCE = AUTONOMOUS_DISTANCE_TO_BALL - 5;

    //CAN IDS
    //Acquisition
    public static final int ACQUISITION_ROLLER_MOTOR_NUMBER = 14;

    //Shooter
    public static final int SHOOTER_FLYWHEEL_MOTOR_ONE_NUMBER = 12;
    public static final int SHOOTER_FLYWHEEL_MOTOR_TWO_NUMBER = 13;
    
    //Driver
    public static final int DRIVE_LEFT_MOTOR_1_NUMBER = 3;
    public static final int DRIVE_LEFT_MOTOR_2_NUMBER = 4;
    public static final int DRIVE_RIGHT_MOTOR_1_NUMBER = 1;
    public static final int DRIVE_RIGHT_MOTOR_2_NUMBER = 2;

    //Conveyor
    public static final int CONVEYOR_MOTOR_NUMBER = 11;

    //Climbing
    public static final int CLIMBER_MOTOR_ONE_NUMBER = 5;
    public static final int CLIMBER_MOTOR_TWO_NUMBER = 6;
    public static final int CLIMBER_BRAKE_FIRST_CHANNEL = 4;
    public static final int CLIMBER_BRAKE_SECOND_CHANNEL = 5;
    public static final double CLIMBER_MOTOR_TRANSITION_DELAY_TIME = 0.05;
    public static final double CLIMBER_ARMS_LOWER_POSITION_LIMIT = -4;

    public static final int PCM_ID = 20;
    public static final int ACQUISITION_ARM_FIRST_CHANNEL = 2;
    public static final int ACQUISITION_ARM_SECOND_CHANNEL = 3;
    
    public static final int CARGO_DOOR_FIRST_CHANNEL = 0;
    public static final int CARGO_DOOR_SECOND_CHANNEL = 1;

    //Joysticks
    public static final int OPERATOR_CONTROLLER = 0;
    public static final int DRIVER_LEFT_STICK = 1;
    public static final int DRIVER_RIGHT_STICK = 2;

    public static final int DRIVE_JOYSTICK_Y_AXIS = 1;

    //Formulas
    public static final double DRIVE_DISTANCE_PER_MOTOR_ROTATION = 6*Math.PI/10.71; //diameterOfWheel*pi/gearRatio
    public static final double CLIMBER_DISTANCE_PER_MOTOR_ROTATION = 1; // 
}
