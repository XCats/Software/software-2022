// Copyright (c) FIRST and other WPILib contributors.

// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.*;
import frc.robot.commands.autonomous.AutoCommandSelector;
import frc.robot.subsystems.*;

import org.xcats.frc.lib.input.XboxButton;
import org.xcats.frc.lib.devices.sensors.impl.XSparkMaxEncoder;
import org.xcats.frc.lib.devices.sensors.impl.XTalonFXEncoder;
import org.xcats.frc.lib.devices.sensors.XEncoder;
import org.xcats.frc.lib.devices.sensors.impl.XEncoderGroup;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer
{
    /** The container for the robot. Contains subsystems, OI devices, and commands. */
    private final XboxController operatorController = new XboxController(Constants.OPERATOR_CONTROLLER);
    private final Joystick driverLeftStick = new Joystick(Constants.DRIVER_LEFT_STICK);
    private final Joystick driverRightStick = new Joystick(Constants.DRIVER_RIGHT_STICK);

    //Motors
    private final MotorController acquisitionRollerMotor = new CANSparkMax(Constants.ACQUISITION_ROLLER_MOTOR_NUMBER, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final MotorController conveyorMotor = new CANSparkMax(Constants.CONVEYOR_MOTOR_NUMBER, CANSparkMax.MotorType.kBrushless);
    private final CANSparkMax shooterFlywheelOneMotor = new CANSparkMax(Constants.SHOOTER_FLYWHEEL_MOTOR_ONE_NUMBER, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax shooterFlywheelTwoMotor = new CANSparkMax(Constants.SHOOTER_FLYWHEEL_MOTOR_TWO_NUMBER, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final WPI_TalonFX firstClimberMotor = new WPI_TalonFX(Constants.CLIMBER_MOTOR_ONE_NUMBER);
    private final WPI_TalonFX secondClimberMotor = new WPI_TalonFX(Constants.CLIMBER_MOTOR_TWO_NUMBER);
    private final CANSparkMax driveLeftMotor1 = new CANSparkMax(Constants.DRIVE_LEFT_MOTOR_1_NUMBER, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax driveLeftMotor2 = new CANSparkMax(Constants.DRIVE_LEFT_MOTOR_2_NUMBER, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax driveRightMotor1 = new CANSparkMax(Constants.DRIVE_RIGHT_MOTOR_1_NUMBER, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax driveRightMotor2 = new CANSparkMax(Constants.DRIVE_RIGHT_MOTOR_2_NUMBER, CANSparkMaxLowLevel.MotorType.kBrushless);

    //Sensors
    private final XSparkMaxEncoder driveLeftEncoder1 = new XSparkMaxEncoder("driveLeftEncoder1", driveLeftMotor1, 
        Constants.DRIVE_DISTANCE_PER_MOTOR_ROTATION, XEncoder.Units.Inches);
    private final XSparkMaxEncoder driveLeftEncoder2 = new XSparkMaxEncoder("driveLeftEncoder2", driveLeftMotor2, 
        Constants.DRIVE_DISTANCE_PER_MOTOR_ROTATION, XEncoder.Units.Inches);
    private final XSparkMaxEncoder driveRightEncoder1 = new XSparkMaxEncoder("driveRightEncoder1", driveRightMotor1, 
        Constants.DRIVE_DISTANCE_PER_MOTOR_ROTATION, XEncoder.Units.Inches);
    private final XSparkMaxEncoder driveRightEncoder2 = new XSparkMaxEncoder("driveRightEncoder2", driveRightMotor2, 
        Constants.DRIVE_DISTANCE_PER_MOTOR_ROTATION, XEncoder.Units.Inches);
    private final XTalonFXEncoder firstClimberEncoder = new XTalonFXEncoder("firstClimberEncoder", firstClimberMotor, 
        Constants.CLIMBER_DISTANCE_PER_MOTOR_ROTATION, XEncoder.Units.Inches);
    private final XTalonFXEncoder secondClimberEncoder = new XTalonFXEncoder("secondClimberEncoder", secondClimberMotor, 
        Constants.CLIMBER_DISTANCE_PER_MOTOR_ROTATION, XEncoder.Units.Inches);

    private final XEncoderGroup driveLeftEncoders = new XEncoderGroup("Left Drive Encoders", driveLeftEncoder1, driveLeftEncoder2);
    private final XEncoderGroup driveRightEncoders = new XEncoderGroup("Right Drive Encoders", driveRightEncoder1, driveRightEncoder2); 
    private final XEncoderGroup climberEncoders = new XEncoderGroup("Climber Encoders", firstClimberEncoder, secondClimberEncoder);

    private final DoubleSolenoid acquisitionArm = new DoubleSolenoid(Constants.PCM_ID, PneumaticsModuleType.CTREPCM,
        Constants.ACQUISITION_ARM_FIRST_CHANNEL, Constants.ACQUISITION_ARM_SECOND_CHANNEL);
    
    private final DoubleSolenoid cargoDoor = new DoubleSolenoid(Constants.PCM_ID, PneumaticsModuleType.CTREPCM, Constants.CARGO_DOOR_FIRST_CHANNEL, 
        Constants.CARGO_DOOR_SECOND_CHANNEL);
    
    private final DoubleSolenoid climberBrake = new DoubleSolenoid(Constants.PCM_ID, PneumaticsModuleType.CTREPCM, Constants.CLIMBER_BRAKE_FIRST_CHANNEL, 
        Constants.CLIMBER_BRAKE_SECOND_CHANNEL);

    // The robot's subsystems and commands are defined here...
    private final AcquisitionSubsystem acquisition = new AcquisitionSubsystem(acquisitionRollerMotor, acquisitionArm);
    private final ShooterSubsystem shooter = new ShooterSubsystem(shooterFlywheelOneMotor);
    private final DriveBaseSubsystem drive = new DriveBaseSubsystem(driveLeftMotor1, driveRightMotor1, driveLeftEncoders, driveRightEncoders);
    private final ConveyorSubsystem conveyor = new ConveyorSubsystem(conveyorMotor, cargoDoor);
    private final ClimberSubsystem climb = new ClimberSubsystem(firstClimberMotor, climberEncoders, climberBrake);

    //Autonomous command mode selector via Shuffleboard
    private final AutoCommandSelector mAutoCommandSelector = new AutoCommandSelector(shooter, conveyor, drive, acquisition);
    
    XboxButton acquisitionRollerButton = new XboxButton(operatorController, XboxController.Button.kA); //A=roller button
    XboxButton shootButton = new XboxButton(operatorController, XboxController.Button.kY); //Y=flywheel button
    XboxButton conveyorMotorOnButton = new XboxButton(operatorController, XboxController.Button.kB);
    XboxButton intakeOnButton = new XboxButton(operatorController, XboxController.Button.kX);  
    XboxButton climberUpButton = new XboxButton(operatorController, XboxController.Button.kLeftBumper);
    XboxButton climberDownButton = new XboxButton(operatorController, XboxController.Button.kRightBumper);
    XboxButton operatorCamerButton = new XboxButton(operatorController, XboxController.Button.kBack);

    JoystickButton driverCameraButton = new JoystickButton(driverRightStick, 7);

/** The container for the robot. Contains subsystems, OI devices, and commands. */
    public RobotContainer()
    {
        // Initialize Network Tables
        initializeNetworkTables();

        // Configure the button bindings
        configureButtonBindings();

        setMotorDefaults();

        setSensorDefaults();

        //set default commands
        drive.setDefaultCommand(new DefaultDriveCommand(drive, driverLeftStick, driverRightStick));
    }

    private void initializeNetworkTables() {
        NetworkTableInstance inst = NetworkTableInstance.getDefault();
        NetworkTable table = inst.getTable("");

        NetworkTableEntry backupCameraEntry = table.getEntry("backupCameraOn");
        if(backupCameraEntry.getNumber(100).intValue() == 100) backupCameraEntry.setNumber(0);
    }
    
    /**
     * Use this method to define your button->command mappings. Buttons can be created by
     * instantiating a {@link GenericHID} or one of its subclasses ({@link
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
     * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings()
    {
        // See https://docs.wpilib.org/en/stable/docs/software/commandbased/binding-commands-to-triggers.html

        acquisitionRollerButton
            .whenPressed(new AcquisitionRollersOnCommand(acquisition))
            .whenReleased(new AcquisitionRollersOffCommand(acquisition));

        conveyorMotorOnButton
            .whenPressed(new ConveyorOnCommand(conveyor))
            .whenReleased(new ConveyorOffCommand(conveyor));

        intakeOnButton
            .whenPressed(new IntakeOnCommand(acquisition, conveyor))
            .whenReleased(new IntakeOffCommand(acquisition, conveyor));

        acquisitionRollerButton
            .whenPressed(new AcquisitionRollersOnCommand(acquisition))
            .whenReleased(new AcquisitionRollersOffCommand(acquisition)); //turns rollers on while button pressed

        shootButton
            .whenPressed(new DeliveryOnCommand(shooter, conveyor))
            .whenReleased(new DeliveryOffCommand(shooter, conveyor)); //turns shooting mechanism on while button pressed

        climberUpButton
            .whenPressed(new ClimberUpCommand(climb, Constants.CLIMBER_MOTOR_TRANSITION_DELAY_TIME))
            .whenReleased(new ClimberStopCommand(climb));

        climberDownButton
            .whenPressed(new ClimberDownCommand(climb))
            .whenReleased(new ClimberArmsOffCommand(climb));

        driverCameraButton.whenPressed(new ToggleCameraCommand());

        operatorCamerButton.whenPressed(new ToggleCameraCommand());
    }
    
    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand()
    {
        //Selected command will execute
        return mAutoCommandSelector.getSelectedCommand();
    }

    private void setMotorDefaults()
    {    
        shooterFlywheelTwoMotor.restoreFactoryDefaults();
        shooterFlywheelOneMotor.restoreFactoryDefaults();
        driveLeftMotor1.restoreFactoryDefaults();
        driveLeftMotor2.restoreFactoryDefaults();
        driveRightMotor1.restoreFactoryDefaults();
        driveRightMotor2.restoreFactoryDefaults();

        //set motor follows
        driveLeftMotor2.follow(driveLeftMotor1);
        driveRightMotor2.follow(driveRightMotor1);
        shooterFlywheelTwoMotor.follow(shooterFlywheelOneMotor, true);
        secondClimberMotor.follow(firstClimberMotor);

        //inversion
        driveRightMotor1.setInverted(true);
        firstClimberMotor.setInverted(true);

        //set motor modes
        firstClimberMotor.setNeutralMode(NeutralMode.Brake);
        secondClimberMotor.setNeutralMode(NeutralMode.Brake);

        shooterFlywheelOneMotor.burnFlash();
        shooterFlywheelTwoMotor.burnFlash();
    }

    public void setSensorDefaults()
    {
        climb.zeroEncoders();
    }

    public void setDefaultDriveCommand()
    {
        drive.setDefaultCommand(new DefaultDriveCommand(drive, driverLeftStick, driverRightStick));
    }
}
